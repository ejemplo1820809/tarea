package com.example.tarea.excepciones;

public class NotFoundException extends RuntimeException{

    private int code;

    private String message;

    public NotFoundException(String message) {
        this.code = 404;
        this.message=message;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
