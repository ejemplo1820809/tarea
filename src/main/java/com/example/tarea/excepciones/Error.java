package com.example.tarea.excepciones;


import lombok.Builder;

@Builder
public class Error {

    private int codigo;
    private String mensaje;

    public Error(int codigo, String mensaje) {
        this.codigo = codigo;
        this.mensaje = mensaje;
    }
}
