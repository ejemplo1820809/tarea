package com.example.tarea.excepciones;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(value= NotFoundException.class)
    public ResponseEntity<Error> notfoundException (NotFoundException notFoundException){
        Error error=Error.builder().codigo(notFoundException.getCode()).mensaje(notFoundException.getMessage()).build();
        return ResponseEntity.badRequest().body(error);
    }
}
