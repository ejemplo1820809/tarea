package com.example.tarea.controller;


import com.example.tarea.entity.Rol;
import com.example.tarea.service.IRolService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RequestMapping("/api/rol")
@RestController
public class RolController {

    @Autowired
    private IRolService iRolService;

    @PostMapping("/add")
    public ResponseEntity<?> save(@Valid @RequestBody Rol rol, BindingResult bindingResult){
        if(bindingResult.hasFieldErrors()){
            return validacion(bindingResult);
        }
        Rol rolNuevo=iRolService.save(rol);
        return ResponseEntity.status(HttpStatus.CREATED).body(rolNuevo);
    }

    @GetMapping("/filtrarNombre/{nombre}")
    public ResponseEntity<Rol> findByName(@RequestParam String nombre){
        Rol rol=iRolService.findByName(nombre);
        return ResponseEntity.ok(rol);

    }

    @GetMapping("/listarRoles")
    public ResponseEntity<List<Rol>> findByName(){
        List<Rol> roles=iRolService.findAll();
        return ResponseEntity.ok(roles);
    }

    //Esto me muestra los errores cuando utilizo el starter de validacion
    private ResponseEntity<Map<String, String>> validacion(BindingResult bindingResult){
        Map<String, String> errores=new HashMap<>();
        bindingResult.getFieldErrors().forEach(error->{
            errores.put(error.getField(), "El campo "+error.getField()+error.getDefaultMessage());
        });
          return ResponseEntity.badRequest().body(errores);
    }
}
