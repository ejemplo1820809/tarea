package com.example.tarea.service;


import com.example.tarea.entity.Rol;
import com.example.tarea.excepciones.NotFoundException;
import com.example.tarea.repository.IRolRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class RolService implements  IRolService {

    @Autowired
    private IRolRepository iRolRepository;


    @Transactional
    public Rol save(Rol rol){
        return iRolRepository.save(rol);
    }

    @Transactional(readOnly = true)
    public List<Rol> findAll(){
        return iRolRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Rol findByName(String name){
        Rol rol= iRolRepository.findByNombreEquals(name);
        if(rol==null){
            throw new NotFoundException("Ese rol no se encuentra en la base de datos");
        }
        return rol;
    }
}
