package com.example.tarea.service;


import com.example.tarea.entity.Tarea;
import com.example.tarea.repository.ITareaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TareaService implements ITareaService{

    @Autowired
    private ITareaRepository iTareaRepository;


    public Tarea save(Tarea tarea){
        return iTareaRepository.save(tarea);

    }

    public List<Tarea> findAll(){
        return iTareaRepository.findAll();
    }

}
