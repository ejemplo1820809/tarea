package com.example.tarea.service;

import com.example.tarea.entity.Rol;

import java.util.List;

public interface IRolService {

    public Rol save(Rol rol);

    public List<Rol> findAll();

    public Rol findByName(String name);


}
