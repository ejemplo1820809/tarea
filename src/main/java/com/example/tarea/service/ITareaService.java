package com.example.tarea.service;

import com.example.tarea.entity.Tarea;

import java.util.List;

public interface ITareaService {

    public Tarea save(Tarea tarea);

    public List<Tarea> findAll();
}
