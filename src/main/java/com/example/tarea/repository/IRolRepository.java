package com.example.tarea.repository;

import com.example.tarea.entity.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface IRolRepository extends JpaRepository<Rol, UUID> {

    public Rol findByNombreEquals(String name);


}
