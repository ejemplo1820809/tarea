package com.example.tarea.repository;

import com.example.tarea.entity.Tarea;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ITareaRepository extends JpaRepository<Tarea, UUID> {
}
