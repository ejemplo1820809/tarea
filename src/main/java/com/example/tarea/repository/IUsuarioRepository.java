package com.example.tarea.repository;

import com.example.tarea.entity.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;


@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, UUID> {
}
