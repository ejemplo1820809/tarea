package com.example.tarea.entity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="rol")
@Entity
public class Rol {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;


    @NotBlank
    @Column(unique = true)
    private String nombre;

    private LocalDateTime fechaCreacion;

    private LocalDateTime fechaActualizacion;

    private LocalDateTime fechaEliminacion;

    @PrePersist
    public void PrePersist(){
        System.out.println("Se actualizo la fecha de creacion");
        this.fechaCreacion= LocalDateTime.now();
    }
    @PreUpdate
    public void PreUpdate(){
        System.out.println("Se actualizo la fecha de actualizacion");
        this.fechaActualizacion=LocalDateTime.now();
    }

    @PreRemove
    public void PreDelete(){
        System.out.println("Se actualizo la fecha de eliminacion");
        this.fechaEliminacion=LocalDateTime.now();
    }

}


