package com.example.tarea.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="tarea")
@Entity
public class Tarea {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column(nullable = false, unique = true, length = 255)
    private String nombreTarea;

    @Column(nullable = false, unique = true, length = 255)
    private String descripcion;

    @Column(nullable = false)
    private int tiempoDuracion;

    private LocalDateTime fechaCreacion;

    private LocalDateTime fechaActualizacion;

    private LocalDateTime fechaEliminacion;

    @ManyToOne
    @JsonIgnoreProperties({"listTarea", "listUsuario"})
    private Proyecto proyecto;

    @ManyToMany
    @JoinTable(name="tarea_usuario", joinColumns = @JoinColumn(name = "ide_tarea"), inverseJoinColumns = @JoinColumn(name="ide_usuario"))
    @JsonIgnoreProperties({"listTarea", "proyecto"})
    private List<Usuario> listUsuario=new ArrayList<>();

    @PrePersist
    public void PrePersist(){
        System.out.println("Se actualizo la fecha de creacion");
        this.fechaCreacion= LocalDateTime.now();
    }

    @PreUpdate
    public void PreUpdate(){
        System.out.println("Se actualizo la fecha de actualizacion");
        this.fechaActualizacion=LocalDateTime.now();
    }

    @PreRemove
    public void PreDelete(){
        System.out.println("Se actualizo la fecha de eliminacion");
        this.fechaEliminacion=LocalDateTime.now();
    }


}
