package com.example.tarea.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.antlr.v4.runtime.misc.NotNull;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name="proyecto")
@Entity
public class Proyecto {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    private LocalDateTime fechaCreacion;

    private LocalDateTime fechaActualizacion;

    private LocalDateTime fechaEliminacion;

    @Column(nullable = false, unique = true, length = 255)
    private String nombre;

    @Column(nullable = false)
    private String descripcion;

    private int tiempoDuracion=0;

    @OneToMany(mappedBy = "proyecto", cascade = CascadeType.REMOVE, orphanRemoval = true)
    @JsonIgnoreProperties({"proyecto", "listUsuario"})
    List<Tarea> listTarea=new ArrayList<>();

    @OneToMany(mappedBy = "proyecto")
    @JsonIgnoreProperties({"proyecto", "listTarea"})
    List<Usuario> listUsuario=new ArrayList<>();

    @PrePersist
    public void PrePersist(){
        System.out.println("Se actualizo la fecha de creacion");
        this.fechaCreacion= LocalDateTime.now();
    }

    @PreUpdate
    public void PreUpdate(){
        System.out.println("Se actualizo la fecha de actualizacion");
        this.fechaActualizacion=LocalDateTime.now();
    }

    @PreRemove
    public void PreDelete(){
        System.out.println("Se actualizo la fecha de eliminacion");
        this.fechaEliminacion=LocalDateTime.now();
    }

}
