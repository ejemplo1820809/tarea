package com.example.tarea.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table(name = "usuario")
@Entity
public class Usuario {

    @GeneratedValue(generator = "UUID")
    @Id
    private UUID ide;

    @Column(nullable = false, unique = true)
    private String nombreUsuario;

    @Column(nullable = false, length = 255)
    private String password;

    private boolean admin=false;

    private LocalDateTime fechaCreacion;

    private LocalDateTime fechaActualizacion;

    private LocalDateTime fechaEliminacion;

    @ManyToMany
    @JoinTable(name="tarea_usuario", joinColumns = @JoinColumn(name = "ide_usuario"), inverseJoinColumns = @JoinColumn(name="ide_tarea"))
    List<Tarea> listTarea=new ArrayList<>();

    @ManyToOne
    private Proyecto proyecto;

    @ManyToMany
    @JoinTable(name="usuario_rol", joinColumns = @JoinColumn(name = "ide_usuario"), inverseJoinColumns = @JoinColumn(name="ide_rol"))
    List<Rol> listroles=new ArrayList<>();

    @PrePersist
    public void PrePersist(){
        System.out.println("Se actualizo la fecha de creacion");
        this.fechaCreacion= LocalDateTime.now();
    }

    @PreUpdate
    public void PreUpdate(){
        System.out.println("Se actualizo la fecha de actualizacion");
        this.fechaActualizacion=LocalDateTime.now();
    }

    @PreRemove
    public void PreDelete(){
        System.out.println("Se actualizo la fecha de eliminacion");
        this.fechaEliminacion=LocalDateTime.now();
    }


}
